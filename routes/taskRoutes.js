const express = require('express')
const router = express.Router()
const TaskController = require(`../controllers/TaskController.js`)

router.get('/:id/retrieve', (request,response) => {
		TaskController.retrieveTask(request.params.id).then((retrieveTask) => 
			response.send(retrieveTask))
})

router.put('/:id/update', (request,response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => 
		response.send(updatedTask))
})


router.post('/:id/status-update', (request,response) => {
	TaskController.updateStatus(request.params.id, request.body).then((task) => response.send(task))
})



module.exports = router 