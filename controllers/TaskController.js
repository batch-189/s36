const Task = require('../models/Task.js')

module.exports.retrieveTask = (taskId) => {
	return Task.findById(taskId).then((retrievedId,error) => {
		if(error){
			return error
		}
		return retrievedId
	})

}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.name = newContent.name
		return result.save().then((updatedTask,error) => {
			if(error){
				return error
			}
			return updatedTask
		})
	})

}

module.exports.updateStatus = (oldStatus,newStatus) => {
	return Task.findOneAndUpdate(oldStatus).then((result,error)=> {
		if(error){
			return error
		}
		result.status = newStatus.status
		return result.save().then((registeredStatus,error) => {
			if(error){
				return error
			}
			return registeredStatus
		})
	})
}